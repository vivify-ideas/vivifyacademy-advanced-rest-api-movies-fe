import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './components/movies/movies.component';
import { SearchPageMovieComponent } from './search/components/search-page-movie/search-page-movie.component';
import { AddMovieComponent } from './components/add-movie/add-movie.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ShowMovieComponent } from './components/show-movie/show-movie.component';
import { MovieResolver } from './shared/resolvers/movie.resolver';
import { AuthGuard } from './shared/guards/auth.guard';
import { GuestGuard } from './shared/guards/guest.guard';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/movies',
        pathMatch: 'full'
    },
    {
        path: 'movies',
        canActivate: [AuthGuard],
        component: MoviesComponent,
    },
    {
        path: 'movies/search/:term',
        canActivate: [AuthGuard],
        component: SearchPageMovieComponent
    },
    {
        path: 'add',
        canActivate: [AuthGuard],
        component: AddMovieComponent
    },
    {
        path: 'login',
        canActivate: [GuestGuard],
        component: LoginComponent
    },
    {
        path: 'register',
        canActivate: [GuestGuard],
        component: RegisterComponent
    },
    {
        path: 'movies/:id',
        canActivate: [AuthGuard],
        component: ShowMovieComponent,
        resolve: {
            movie: MovieResolver
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ]
}) export class AppRoutingModule { }