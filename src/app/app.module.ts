import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { SearchModule } from './search/search.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { MoviesComponent } from './components/movies/movies.component';
import { OrderModule } from 'ngx-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddMovieComponent } from './components/add-movie/add-movie.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ShowMovieComponent } from './components/show-movie/show-movie.component';

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        MoviesComponent,
        AddMovieComponent,
        LoginComponent,
        RegisterComponent,
        ShowMovieComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        SearchModule,
        OrderModule,
        NgxPaginationModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
