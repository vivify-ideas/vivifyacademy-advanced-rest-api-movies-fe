import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../shared/services/movie.service';
import { Movie } from '../../shared/models/movie.model';

@Component({
    selector: 'app-show-movie',
    templateUrl: './show-movie.component.html',
})
export class ShowMovieComponent implements OnInit {

    private movie: Movie;

    constructor(
        private route: ActivatedRoute,
        private movieService: MovieService) {
    }

    public ngOnInit() {
        this.route.data
            .subscribe((data: { movie: Movie }) => {
                this.movie = data.movie;
            });
    }
}
