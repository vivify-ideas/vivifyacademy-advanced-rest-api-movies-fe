import { Component, OnInit } from '@angular/core';
import { Movie } from '../../shared/models/movie.model';
import { MovieService } from '../../shared/services/movie.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-add-movie',
    templateUrl: './add-movie.component.html',
})
export class AddMovieComponent implements OnInit {

    public movie: Movie = new Movie();
    public genres: String = '';

    constructor(
        private movieService: MovieService,
        private router: Router
    ) { }

    public ngOnInit() {
    }

    public submit() {
        this.movie.genres = this.genres.split(',').map(genre => genre.trim());
        this.movieService.addMovie(this.movie).subscribe(() => {
            this.router.navigateByUrl('/');
        });
    }
    public trackByIndex(index: number, obj: any): any {
        return index;
    }

}
