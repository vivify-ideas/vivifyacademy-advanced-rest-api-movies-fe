import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../shared/services/movie.service';
import { Movie } from '../../shared/models/movie.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
})
export class MoviesComponent implements OnInit {

    private movies: Movie[] = [];
    public counter = 0;
    public selectedAll = false;
    public selectedAny = false;
    private order = 'name';
    private reverse = false;

    constructor(private movieService: MovieService) { }

    public ngOnInit() {
        this.movieService.getMovies().subscribe(data => {
            this.movies = data;
        }, (err: HttpErrorResponse) => {
            alert(`Backend returned code ${err.status} with message: ${err.error}`);
        });
    }

    // install pipe with npm install  ngx-order-pipe --save
    // documentation https://github.com/VadimDez/ngx-order-pipe

    // install pipe with npm install ngx-pagination --save
    // documentation https://www.npmjs.com/package/ngx-pagination

    public setOrder(value: string) {
        if (this.order === value) {
            this.reverse = !this.reverse;
        }

        this.order = value;
    }

    public onVoted(agreed: boolean) {
        this.counter++;
        this.selectedAny = true;
    }

    public selectAllCounter() {
        this.counter = this.movies.length;
    }

    public deselectAllCounter() {
        this.counter = 0;
    }
}
