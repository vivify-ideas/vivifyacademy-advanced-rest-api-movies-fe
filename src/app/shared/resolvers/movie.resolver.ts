import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MovieService } from './../services/movie.service';

@Injectable()
export class MovieResolver implements Resolve<any> {
    constructor(private movieService: MovieService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        let id = parseInt(route.paramMap.get('id'));

        return this.movieService.getMovieById(id);
    }
}
