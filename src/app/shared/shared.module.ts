import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieService } from './services/movie.service';
import { MovieRowComponent } from './components/movie-row/movie-row.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { MovieResolver } from './resolvers/movie.resolver';
import { AuthGuard } from './guards/auth.guard';
import { GuestGuard } from './guards/guest.guard';

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        MovieService,
        AuthService,
        MovieResolver,
        AuthGuard,
        GuestGuard
    ],
    declarations: [
        MovieRowComponent
    ],
    exports: [
        MovieRowComponent,
    ],
})
export class SharedModule { }
