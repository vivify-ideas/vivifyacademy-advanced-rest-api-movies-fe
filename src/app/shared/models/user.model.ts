export class User
{
  public name = '';
  public email = '';
  public password = '';
  public confirmPassword = '';

  constructor( data ?: {
    name ?: string,
    email ?: string,
    password ?: string,
    confirmPassword ?: string,
  }) {
    Object.assign(this, data || {});
  }
}