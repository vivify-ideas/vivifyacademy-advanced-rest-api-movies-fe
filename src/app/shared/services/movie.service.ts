import { Injectable } from '@angular/core';
import { Movie } from './../models/movie.model';
import { Observable, Observer } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class MovieService {

    private movies: Movie[] = [];

    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) { }

    public getMovies() {
        return new Observable((o: Observer<any>) => {
            this.http.get('http://127.0.0.1:8000/api/movies', {
                headers: this.authService.getRequestHeaders()
            }).subscribe((movies: any[]) => {
                this.movies = movies.map((movie) => {
                    return new Movie(
                        movie.id,
                        movie.name,
                        movie.director,
                        movie.image_url,
                        movie.duration,
                        movie.release_date,
                        movie.genres);
                });
                o.next(this.movies);
                return o.complete();
            });
        });
    }

    public getMovieById(id: number) {
        return new Observable((o: Observer<any>) => {
            this.http.get('http://localhost:8000/api/movies/' + id, {
                headers: this.authService.getRequestHeaders()
            }).subscribe((movie: any) => {
                let newMovie = new Movie(
                    movie.id,
                    movie.name,
                    movie.director,
                    movie.image_url,
                    movie.duration,
                    movie.release_date,
                    movie.genres,
                );
                o.next(newMovie);
                return o.complete();
            });
        });
    }

    public searchMoviesByTerm(term) {
        return new Observable((o: Observer<any>) => {
            let params = new HttpParams();
            params = params.append('term', term);

            this.http.get('http://localhost:8000/api/movies', {
                params: params,
                headers: this.authService.getRequestHeaders()
            }).subscribe((movies: any) => {
                this.movies = movies.map((movie) => {
                    return new Movie(
                        movie.id,
                        movie.name,
                        movie.director,
                        movie.image_url,
                        movie.duration,
                        movie.release_date,
                        movie.genres);
                });
                o.next(this.movies);
                return o.complete();
            });
        });
    }

    public addMovie(movie: Movie) {
        return new Observable((o: Observer<any>) => {
            this.http.post('http://localhost:8000/api/movies', {
                name: movie.name,
                director: movie.director,
                image_url: movie.image_url,
                duration: movie.duration,
                release_date: movie.release_date,
                genres: movie.genres
            }, {
                    headers: this.authService.getRequestHeaders()
                }).subscribe((movies: any) => {
                    const movie = new Movie(
                        movies.name,
                        movies.director,
                        movies.image_url,
                        movies.duration,
                        movies.release_date,
                        movies.genres);
                    this.movies.push(movie);
                    o.next(movie);
                    return o.complete();
                }, (err: HttpErrorResponse) => {
                    alert(`Backend returned code ${err.status} with message: ${err.error}`);
                }
            );
        });
    }
}
