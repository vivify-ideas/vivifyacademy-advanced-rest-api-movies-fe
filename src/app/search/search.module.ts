import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { FormsModule } from '@angular/forms';
import { SearchPageMovieComponent } from './components/search-page-movie/search-page-movie.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        SearchInputComponent,
        SearchPageMovieComponent
    ],
    exports: [
        SearchInputComponent,
        SearchPageMovieComponent
    ]
})
export class SearchModule { }
