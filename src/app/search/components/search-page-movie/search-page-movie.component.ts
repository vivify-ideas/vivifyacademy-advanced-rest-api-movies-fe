import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../../shared/services/movie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../../../shared/models/movie.model';

@Component({
  selector: 'app-search-page-movie',
  templateUrl: './search-page-movie.component.html'
})
export class SearchPageMovieComponent implements OnInit {

    private movies: Movie[];
    private term;

    constructor(private movieService: MovieService,
                private route: ActivatedRoute,
                private router: Router,
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.movieService.searchMoviesByTerm(params.term).subscribe(data => {
                this.movies = data;
                this.term = params.term;
            });
        });
    }

}
